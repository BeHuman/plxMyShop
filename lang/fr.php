<?php

$LANG = array(

'L_PAGE_TITLE'				=> 'Création et édition des produits',

'L_DEFAULT_NEW_PRODUCT_URL'			=> 'nouveau-produit',
'L_CONFIG_INFOS_NB_PRODUCTS'		=> 'Nombre de produits :',
'L_CONFIG_PRIX_BASE'				=> 'Prix de base :',
'L_MENU_PRODUCTS_TITLE'				=> 'Liste et modification des produits',
'L_MENU_PRODUCTS'					=> 'Liste des produits',
'L_MENU_CATS'					=> 'Liste des catégories',
'L_MENU_ORDERS'					=> 'Liste des commandes',
'L_MENU_CONFIG'					=> 'Configuration',

'L_DATE'                        => 'Date',
'L_PAIEMENT'                        => 'Moyen de paiement',
'L_MONTANT'                        => 'Montant',
'L_ACTIONS'                        => 'Actions',

'L_EMAIL'		   			=> 'Destinataire(s) de l\'e-mail *',
'L_EMAIL_CC'	   			=> 'Destinataire(s) en copie de l\'e-mail ',
'L_EMAIL_BCC'	   			=> 'Destinataire(s) en copie cachée de l\'e-mail ',
'L_EMAIL_SUBJECT'  			=> 'Objet de l\'e-mail',
'L_DEFAULT_OBJECT'			=> 'Récapitulatif de commande',
'L_DEFAULT_CONFIRMATION_OBJECT'			=> 'Confirmation de commande',
'L_TEMPLATE'				=> 'Template',
'L_UNKNOWN_PRODUCT'				=> 'Ce produit n\'existe pas ou n\'existe plus !',
'L_PRODUCTCONTENT_INPROCESS'	=> 'Ce produit est actuellement en cours de rédaction',
# products.php

'L_PRODUCTS_PAGE_TITLE'				=> 'Création et édition des produits',
'L_PRODUCTS_ID'						=> 'Identifiant',
'L_PRODUCTS_GROUP'					=> 'Groupe',
'L_PRODUCTS_TITLE'					=> 'Titre',
'L_PRODUCTS_URL'					=> 'Url',
'L_PRODUCTS_ACTIVE'					=> 'Active',
'L_PRODUCTS_ORDER'					=> 'Ordre',
'L_PRODUCTS_MENU'					=> 'Menu',
'L_PRODUCTS_ACTION'					=> 'Action',
'L_PRODUCTS_TEMPLATE_FIELD'			=> 'Template',
'L_PRODUCTS_PAGE_HOME'				=> 'Définir en tant que produit d\'accueil',
'L_PRODUCTS_HOME_PAGE' 			 	=> 'Produit<br />d\'accueil',
'L_PRODUCTS_SRC_TITLE'				=> 'Éditer le code source de ce produit',
'L_PRODUCTS_SRC'					=> 'Éditer',
'L_PRODUCTS_NEW_PAGE'				=> 'Nouveau produit',
'L_PRODUCTS_UPDATE'					=> 'Modifier la liste des produits',
'L_ERR_PRODUCT_ALREADY_EXISTS'		=> 'Titre du produit déjà utilisé',
'L_PRODUCTS_IMAGE'                  => 'Image de présentation',
'L_PRODUCTS_IMAGE_CHOICE'           => 'Choisir une image',
'L_PRODUCTS_SHORTCODE'              => 'shortcode utilisable dans une page statique',
'L_PRODUCTS_BASKET_BUTTON'          => 'Afficher le bouton "Ajouter au panier"',
'L_PRODUCTS_BASKET_NO_BUTTON'       => 'Afficher un message si le bouton "Ajouter au panier" n\'est pas affiché',
'L_PRODUCTS_CATEGORIES'             => 'Catégories de ce produit',
'L_PRODUCTS_PRICE'                  => 'Prix T.T.C.',
'L_PRODUCTS_WEIGHT'                 => 'Poids (kg)',



# product.php

'L_PRODUCT_BACK_TO_PAGE'			=> 'Retour à la liste des produits',
'L_CAT_BACK_TO_PAGE'			    => 'Retour à la liste des catégories',
'L_PRODUCT_UNKNOWN_PAGE'			=> 'Ce produit n\'existe pas ou n\'existe plus !',
'L_PRODUCT_TITLE'					=> 'Édition du produit',
'L_CAT_TITLE'					    => 'Édition de la catégorie',

'L_PRODUCT_VIEW_PAGE_ON_SITE'				=> 'Visualiser le produit %s sur le site',
'L_CAT_VIEW_PAGE_ON_SITE'				    => 'Visualiser la catégorie de produit %s sur le site',

'L_PRODUCT_UPDATE'					=> 'Enregistrer ce produit',
'L_CAT_UPDATE'					    => 'Enregistrer cette catégorie',
'L_PRODUCT_TITLE_HTMLTAG'			=> 'Contenu balise title (option)',
'L_PRODUCT_META_DESCRIPTION'		=> 'Contenu de la balise meta "description" pour ce produit (option)',
'L_CAT_META_DESCRIPTION'		    => 'Contenu de la balise meta "description" pour cette catégorie (option)',
'L_PRODUCT_META_KEYWORDS'			=> 'Contenu de la balise meta "keywords" pour ce produit (option)',
'L_CAT_META_KEYWORDS'			    => 'Contenu de la balise meta "keywords" pour cette catégorie (option)',

#create product/cat
'CREATE_PRODUCTS'              => 'Création et édition des produits',
'CREATE_CATS'              => 'Création et édition des catégories',

'L_CAT'              => 'Catégorie',

#commandes
'LIST_ORDERS'                                  => 'Liste des commandes',

#Related to panier
'L_PANIER_POS_BOTTOM'               => 'En bas des pages de catégories et des produits',
'L_PANIER_POS_SEPARATE'             => 'Sur une page séparée',
'L_PANIER_POS_BOTH'                 => 'En bas des pages et sur une page séparée',

#config page
'L_CONFIG_SHOP_INFO'                     => 'Informations Boutique',
'L_CONFIG_SHOP_NAME'                     => 'Nom de la boutique',
'L_CONFIG_SHOP_OWNER'                     => 'Nom et prénom du commerçant',
'L_CONFIG_SHOP_STREET'                     => 'Rue du commerçant',
'L_CONFIG_SHOP_ZIP'                     => 'Code postal du commerçant',
'L_CONFIG_SHOP_TOWN'                     => 'Ville du commerçant',
'L_CONFIG_SHOP_CURRENCY'                     => 'Devise',
'L_CONFIG_POSITION_CURRENCY'            => 'Position de la devise',
'L_BEFORE'                              => 'Avant le prix',
'L_AFTER'                              => 'Après le prix',
'L_CONFIG_SECURITY'                 => 'Sécurité',
'L_CONFIG_SECURITY_KEY'                 => 'Clé de chiffrement',

'L_CONFIG_DELIVERY_TITLE'                 => 'Configuration des moyens de livraison et paiement',
'L_CONFIG_DELIVERY_SHIPPING'                 => 'Livraison par "SoColissimo Recommandé"',
'L_CONFIG_DELIVERY_CONFIG'                 => 'Configuration "SoColissimo Recommandé"',
'L_CONFIG_DELIVERY_RECORDED'                 => 'Accusé de réception',
'L_CONFIG_DELIVERY_WEIGHT'                 => 'Poids en kg',

'L_CONFIG_PAYMENT_CHEQUE'                 => 'Paiment par chèque',
'L_CONFIG_PAYMENT_CASH'                 => 'Paiement en argent comptant',
'L_CONFIG_PAYMENT_PAYPAL'                 => 'Paiment par PAYPAL',
'L_CONFIG_CONF_PAYPAL'                 => 'Configuration Paypal',
'L_CONFIG_EMAIL_PAYPAL'                 => 'Adresse e-mail Paypal',
'L_CONFIG_CURRENCY_PAYPAL'                 => 'Code Devise',
'L_CONFIG_RETURN_URL_PAYPAL'                 => 'URL de retour',
'L_CONFIG_CANCEL_URL_PAYPAL'                 => 'URL d\'annulation',

'L_CONFIG_EMAIL_ORDER_TITLE'                 => 'Configuration e-mail de commande',
'L_CONFIG_EMAIL_ORDER_SUBJECT_CUST'                 => 'Titre e-mail "Récapitulatif de commande" (pour le client)',
'L_CONFIG_EMAIL_ORDER_SUBJECT_SHOP'                 => 'Titre e-mail "Nouvelle commande" (pour le commerçant)',

'L_CONFIG_MENU_TITLE'                       => 'Configuration du menu',
'L_CONFIG_MENU_POSITION'                       => 'Position dans le menu des catégories et pages fixes (panier)',

'L_CONFIG_PAGE'                       => 'Configuration des pages',

'L_CONFIG_BASKET_DISPLAY'              => 'Affichage du panier',
'L_CONFIG_PAGE_TEMPLATE'              => 'Template pour les pages fixes et template par défaut des catégories et produits',

'L_CONFIG_SUBMIT'                       => 'Sauvegarder',

'L_ADMIN_MODIFY'                        => 'Modifier la liste des',

'L_PRODUCTS'                            => 'produits',
'L_CATEGORIES'                            => 'catégories',
'L_NEW_PRODUCT'                            => 'Nouveau produit',
'L_NEW_CATEGORY'                            => 'Nouvelle catégorie',

'L_ADMIN_ORDER_VIEW'                    => 'Voir',
'L_ADMIN_ORDER_DELETE'                    => 'Supprimer',
'L_ADMIN_CONFIRM_DELETE'                    => 'Confirmez-vous la suppression de cette commande ?',

#plxMyShop.php
'L_PAYMENT_CHEQUE'                      => 'Chèque',
'L_PAYMENT_CASH'                      => 'Cash',
'L_PAYMENT_PAYPAL'                      => 'Paypal',

'L_EMAIL_SUBJECT'                          => 'Nouvelle commande ',
'L_EMAIL_TEL'                          => 'Tel :',
'L_EMAIL_NOGIFT'                          => 'La commande n\'est pas un cadeau',
'L_EMAIL_GIFT_FOR'                      => 'La commande est un cadeau pour',
'L_EMAIL_PRODUCTLIST'                   => 'Liste des produits',
'L_EMAIL_TOTAL'                   => 'Total (frais de port inclus)',
'L_EMAIL_WEIGHT'                   => 'Poids',
'L_EMAIL_DELIVERY_COST'                   => 'Frais de port',
'L_EMAIL_COMMENT'                   => 'Commentaire',
'L_EMAIL_CONFIRM_PAYPAL'                   => 'La commande est confirmée et en cours de validation de votre part sur Paypal.',
'L_EMAIL_CONFIRM_CHEQUE'                   => 'La commande a bien été confirmée et envoyée par e-mail.',
'L_EMAIL_CONFIRM_CASH'                   => 'La commande a bien été confirmée et envoyée par e-mail.',

'L_EMAIL_CUST_SUBJECT'                          => 'Récapitulatif de la commande ',
'L_EMAIL_CUST_MESSAGE1'                          => 'Vous venez de confirmer une commande sur',
'L_EMAIL_CUST_MESSAGE2'                          => 'Cette commande est en',
'L_WAITING'                                => 'attente',
'L_ONGOING'                                => 'cours',
'L_EMAIL_CUST_MESSAGE3'                          => 'de règlement',
'L_EMAIL_CUST_CHEQUE'                          => 'Pour finaliser cette commande veuillez établir le chèque à l\'ordre de',
'L_EMAIL_CUST_SENDCHEQUE'                          => 'Envoyer votre chèque à cette adresse',
'L_EMAIL_CUST_CASH'                          => 'Veuillez effectuer le paiement à la livraison',
'L_EMAIL_CUST_PAYPAL'                          => 'Cette commande sera finalisée une fois le paiement Paypal contrôlé.',
'L_EMAIL_CUST_SUMMARY'                          => 'Récapitulatif de votre commande',
'L_EMAIL_CUST_ADDRESS'                          => 'Adresse de livraison',
'L_EMAIL_CUST_PAYMENT'                      => 'Méthode de paiement',

'L_EMAIL_SENT1'                      => 'Un e-mail de récapitulatif de commande vous a été envoyé.',
'L_EMAIL_SENT2'                      => 'Si l\'e-mail de récapitulatif de commande n\'apparait pas dans votre liste d\'e-mails en attente ou que celui-ci est signalé en tant que spam, veuillez ajouter %s à votre liste de contacts.', // %s : adresse e-mail du gestionnaire des commandes

'L_EMAIL_ERROR1'                      => 'Une erreur s\'est produite lors de l\'envoi de votre e-mail récapitulatif.',
'L_EMAIL_ERROR2'                      => 'Une erreur s\'est produite lors de l\'envoi de la commande par e-mail.',

'L_FILE_ORDER'                      => 'Commande du ',

'L_FOR'                                => 'pour',
'L_DEL'                                => 'Retirer',

'L_MISSING_EMAIL'                      => 'l\'adresse e-mail n\'est pas définie.',
'L_MISSING_FIRSTNAME'                      => 'Le prénom n\'est pas défini.',
'L_MISSING_LASTNAME'                      => 'Le nom de famille n\'est pas défini.',
'L_MISSING_ADDRESS'                      => 'L\'adresse n\'est pas définie.',
'L_MISSING_ZIP'                      => 'Le code postal n\'est pas défini.',
'L_MISSING_TOWN'                      => 'La ville n\'est pas définie.',
'L_MISSING_COUNTRY'                      => 'Le pays n\'est pas défini.',
'L_MISSING_GIFTNAME'                      => 'Le nom du destinataire du cadeau n\'est pas défini.',

#Espace public
'L_PUBLIC_ADDBASKET'                    => 'Produit ajouté au panier',
'L_PUBLIC_BASKET'                    => 'Votre panier',
'L_PUBLIC_BASKET_NIL'                    => '0.00',
'L_PUBLIC_TOTAL_BASKET'                    => 'Total',
'L_PUBLIC_NOPRODUCT'                    => 'Aucun produit pour le moment.',
'L_PUBLIC_MANDATORY_FIELD'                    => '* = champs obligatoire',
'L_PUBLIC_FIRSTNAME'                    => 'Prénom',
'L_PUBLIC_LASTNAME'                    => 'Nom',
'L_PUBLIC_EMAIL'                    => 'Adresse e-mail',
'L_PUBLIC_TEL'                    => 'Téléphone :',
'L_PUBLIC_ADDRESS'                    => 'Adresse postale',
'L_PUBLIC_ZIP'                    => 'Code postal',
'L_PUBLIC_TOWN'                    => 'Ville',
'L_PUBLIC_COUNTRY'                    => 'Pays',
'L_PUBLIC_GIFT'                    => 'S\'agit-il d\'un cadeau ?',
'L_PUBLIC_GIFTNAME'                    => 'Merci d\'indiquer le prénom et le nom de la personne destinataire du cadeau',
'L_PUBLIC_COMMENT'                   => 'Votre commentaire :',
'L_PUBLIC_VALIDATE_ORDER'                   => 'Validez la commande',
'L_PUBLIC_ADD_BASKET'                   => 'Ajouter au panier',
'L_PUBLIC_TAX'                   => 'T.T.C.',

#panier.js
'L_TOTAL_BASKET'                    => 'Total (frais de port inclus)',
'L_TITRE_PANIER'                    => 'Panier',


);
