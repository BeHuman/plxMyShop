<?php

$LANG = array(

'L_PAGE_TITLE'				=> 'Creacion e modificacion dels produits',

'L_DEFAULT_NEW_PRODUCT_URL'			=> 'nòu-produit',
'L_CONFIG_INFOS_NB_PRODUCTS'		=> 'Nombre de produits :',
'L_MENU_PRODUCTS_TITLE'				=> 'Lista e modificacion dels produits',
'L_MENU_PRODUCTS'					=> 'Lista dels produits',
'L_MENU_CATS'					=> 'Lista de las categorias',
'L_MENU_ORDERS'					=> 'Lista de las comandas',
'L_MENU_CONFIG'					=> 'Configuracion',

'L_DATE'                        => 'Data',
'L_PAIEMENT'                        => 'Mejan de pagament',
'L_MONTANT'                        => 'Montant',
'L_ACTIONS'                        => 'Accions',

'L_EMAIL'		   			=> 'Destinatari(s) del corrièl *',
'L_EMAIL_CC'	   			=> 'Destinatari(s) en copia del corrièl ',
'L_EMAIL_BCC'	   			=> 'Destinatari(s) en copia amagada del corrièl ',
'L_EMAIL_SUBJECT'  			=> 'Objecte del corrièl',
'L_DEFAULT_OBJECT'			=> 'Resumit de la comanda',
'L_DEFAULT_CONFIRMATION_OBJECT'			=> 'Confirmacion de la comanda',
'L_TEMPLATE'				=> 'Modèl',
'L_UNKNOWN_PRODUCT'				=> 'Aqueste produit existís pas o pas mai !',
'L_PRODUCTCONTENT_INPROCESS'	=> 'Aqueste produit es a èsser escrich pel moment',
# products.php

'L_PRODUCTS_PAGE_TITLE'				=> 'Creacion e edicion dels produits',
'L_PRODUCTS_ID'						=> 'Identificant',
'L_PRODUCTS_GROUP'					=> 'Grop',
'L_PRODUCTS_TITLE'					=> 'Títol',
'L_PRODUCTS_URL'					=> 'Url',
'L_PRODUCTS_ACTIVE'					=> 'Activa',
'L_PRODUCTS_ORDER'					=> 'Ordre',
'L_PRODUCTS_MENU'					=> 'Menú',
'L_PRODUCTS_ACTION'					=> 'Accion',
'L_PRODUCTS_TEMPLATE_FIELD'			=> 'Modèl',
'L_PRODUCTS_PAGE_HOME'				=> 'Definir coma produit d\'acuèlh',
'L_PRODUCTS_HOME_PAGE' 			 	=> 'Produit<br />d\'acuèlh',
'L_PRODUCTS_SRC_TITLE'				=> 'Modificar lo còdi sorga d\'aqueste produit',
'L_PRODUCTS_SRC'					=> 'Modificar',
'L_PRODUCTS_NEW_PAGE'				=> 'Nòu produit',
'L_PRODUCTS_UPDATE'					=> 'Modificar la lista dels produits',
'L_ERR_PRODUCT_ALREADY_EXISTS'		=> 'Títol del produit ja utilizat',
'L_PRODUCTS_IMAGE'                  => 'Imatge de presentacion',
'L_PRODUCTS_IMAGE_CHOICE'           => 'Causir un imatge',
'L_PRODUCTS_SHORTCODE'              => 'acorchi utilisable dins una pagina estatica',
'L_PRODUCTS_BASKET_BUTTON'          => 'Afichar lo boton "Ajutar aa panièr"',
'L_PRODUCTS_BASKET_NO_BUTTON'       => 'Afichar un messatge se lo boton "Ajutar al panièr" es pas afichat',
'L_PRODUCTS_CATEGORIES'             => 'Categorias d\'aqueste produit',
'L_PRODUCTS_PRICE'                  => 'Prètz T.T.C.',
'L_PRODUCTS_WEIGHT'                 => 'Pes (kg)',



# product.php

'L_PRODUCT_BACK_TO_PAGE'			=> 'Tornar a la lista dels produits',
'L_CAT_BACK_TO_PAGE'			    => 'Tornar a la lista de las categorias',
'L_PRODUCT_UNKNOWN_PAGE'			=> 'Aqueste produit existís pas o existís pas mai !',
'L_PRODUCT_TITLE'					=> 'Modificacion del produit',
'L_CAT_TITLE'					    => 'Modificacion de la categoria',

'L_PRODUCT_VIEW_PAGE_ON_SITE'		=> 'Visualizar lo produit',
'L_CAT_VIEW_PAGE_ON_SITE'			=> 'Visualizar la categoria del produit',

'L_PRODUCT_ON_SITE'					=> 'sul site',
'L_PRODUCT_UPDATE'					=> 'Enregistrar aqueste produit',
'L_CAT_UPDATE'					    => 'Enregistrar aquesta categoria',
'L_PRODUCT_TITLE_HTMLTAG'			=> 'Contengut de la balisa title (opcion)',
'L_PRODUCT_META_DESCRIPTION'		=> 'Contengut de la balisa meta "description" per aqueste produit (opcion)',
'L_CAT_META_DESCRIPTION'		    => 'Contengut de la balisa meta "description" per aquesta categoria (opcion)',
'L_PRODUCT_META_KEYWORDS'			=> 'Contengut de la balisa meta "keywords" per aqueste produit (opcion)',
'L_CAT_META_KEYWORDS'			    => 'Contengut de la balisa meta "keywords" per aquesta categoria (opcion)',
#create product/cat
'CREATE_PRODUCTS'                   => 'Creacion e modificacion dels produits',
'CREATE_CATS'                       => 'Creacion e modificacion de las categorias',

'L_CAT'              => 'Categoria',

#commandes
'LIST_ORDERS'                                  => 'Lista de las comandas',

#Related to panier
'L_PANIER_POS_BOTTOM'               => 'Aval de las paginas de categorias e dels produits',
'L_PANIER_POS_SEPARATE'             => 'Sus una pagina a despart',
'L_PANIER_POS_BOTH'                 => 'Aval de las paginas e sus una pagina a despart',

#config page
'L_CONFIG_SHOP_INFO'                     => 'Informacions Botiga',
'L_CONFIG_SHOP_NAME'                     => 'Nom de la botiga',
'L_CONFIG_SHOP_OWNER'                     => 'Nom e prenom del comerciant',
'L_CONFIG_SHOP_STREET'                     => 'Carrièra del comerciant',
'L_CONFIG_SHOP_ZIP'                     => 'Còdi postal del comerciant',
'L_CONFIG_SHOP_TOWN'                     => 'Vila del comerciant',
'L_CONFIG_SHOP_CURRENCY'                     => 'Devisa',
'L_CONFIG_POSITION_CURRENCY'            => 'Posicion de la devisa',
'L_BEFORE'                              => 'Abans lo prètz',
'L_AFTER'                              => 'Aprèp lo prètz',
'L_CONFIG_SECURITY'                 => 'Seguretat',
'L_CONFIG_SECURITY_KEY'                 => 'Clau de chiframent',

'L_CONFIG_DELIVERY_TITLE'                 => 'Configuracion dels mejans de liurason e pagament',
'L_CONFIG_DELIVERY_SHIPPING'                 => 'Liurason per "SoColissimo Recommandé"',
'L_CONFIG_DELIVERY_CONFIG'                 => 'Configuracion "SoColissimo Recommandé"',
'L_CONFIG_DELIVERY_RECORDED'                 => 'Acusat de recepcion',
'L_CONFIG_DELIVERY_WEIGHT'                 => 'Pes en kg',

'L_CONFIG_PAYMENT_CHEQUE'                 => 'Pagament per chèc',
'L_CONFIG_PAYMENT_CASH'                 => 'Pagament en argent comptant',
'L_CONFIG_PAYMENT_PAYPAL'                 => 'Pagament per PAYPAL',
'L_CONFIG_CONF_PAYPAL'                 => 'Configuracion Paypal',
'L_CONFIG_EMAIL_PAYPAL'                 => 'Adreça de corrièl Paypal',
'L_CONFIG_CURRENCY_PAYPAL'                 => 'Còdi Devisa',
'L_CONFIG_RETURN_URL_PAYPAL'                 => 'URL de retorn',
'L_CONFIG_CANCEL_URL_PAYPAL'                 => 'URL d\'anullacion',

'L_CONFIG_EMAIL_ORDER_TITLE'                 => 'Configuracion corrièl de comanda',
'L_CONFIG_EMAIL_ORDER_SUBJECT_CUST'                 => 'Títol corrièl "Resumimt de comanda" (pel client)',
'L_CONFIG_EMAIL_ORDER_SUBJECT_SHOP'                 => 'Títol corrièl "Nòva comanda" (pel comerciant)',

'L_CONFIG_MENU_TITLE'                       => 'Configuracion del menú',
'L_CONFIG_MENU_POSITION'                       => 'Posicion dins lo menú de las categorias e paginas fixas (panièr)',

'L_CONFIG_PAGE'                       => 'Configuracion de las paginas',

'L_CONFIG_BASKET_DISPLAY'              => 'Afichatge del panièr',
'L_CONFIG_PAGE_TEMPLATE'              => 'Modèl per las paginas fixas e modèl per defaut de las categorias e produits',

'L_CONFIG_SUBMIT'                       => 'Salvagardar',

'L_ADMIN_MODIFY'                        => 'Modificar la lista',

'L_PRODUCTS'                            => 'dels produits',
'L_CATEGORIES'                            => 'de las categorias',
'L_NEW_PRODUCT'                            => 'Nòu produit',
'L_NEW_CATEGORY'                            => 'Nòva categoria',

'L_ADMIN_ORDER_VIEW'                    => 'Veire',
'L_ADMIN_ORDER_DELETE'                    => 'Suprimir',
'L_ADMIN_CONFIRM_DELETE'                    => 'Confirmatz la supression d\'aquesta comanda ?',

#plxMyShop.php
'L_PAYMENT_CHEQUE'                      => 'Chèc',
'L_PAYMENT_CASH'                      => 'Pagar sulcòp',
'L_PAYMENT_PAYPAL'                      => 'Paypal',

'L_EMAIL_SUBJECT'                          => 'Novèla comanda ',
'L_EMAIL_TEL'                          => 'Tel :',
'L_EMAIL_NOGIFT'                          => 'La comanda es pas un present',
'L_EMAIL_GIFT_FOR'                      => 'La comanda es un present per',
'L_EMAIL_PRODUCTLIST'                   => 'Lista dels produits',
'L_EMAIL_TOTAL'                   => 'Total (fraisses de pòrt enclús)',
'L_EMAIL_WEIGHT'                   => 'Pes',
'L_EMAIL_DELIVERY_COST'                   => 'Fraisses de pòrt',
'L_EMAIL_COMMENT'                   => 'Comentari',
'L_EMAIL_CONFIRM_PAYPAL'                   => 'La comanda es confirmada e es a èsser validada del vòstre costat sus Paypal.',
'L_EMAIL_CONFIRM_CHEQUE'                   => 'La comanda es ben estada confirmada e mandada per corrièl.',
'L_EMAIL_CONFIRM_CASH'                   => 'La comanda es ben estada confirmada e mandada per corrièl.',

'L_EMAIL_CUST_SUBJECT'                          => 'Resumit de la comanda ',
'L_EMAIL_CUST_MESSAGE1'                          => 'Avètz confirmat una comanda sus',
'L_EMAIL_CUST_MESSAGE2'                          => 'Aquesta comanda es en',
'L_WAITING'                                => 'espèra',
'L_ONGOING'                                => 'cors',
'L_EMAIL_CUST_MESSAGE3'                          => 'de pagament',
'L_EMAIL_CUST_CHEQUE'                          => 'Per acabar aquesta comanda mercés de far un chèc a l\'òrdre de',
'L_EMAIL_CUST_SENDCHEQUE'                          => 'Mandatz vòstre chèc a-n aquesta adreça',
'L_EMAIL_CUST_CASH'                          => 'Mercés de pagar a la liurason',
'L_EMAIL_CUST_PAYPAL'                          => 'Aquesta comanda sera terminada un còp lo pagament Paypal contrarotlat.',
'L_EMAIL_CUST_SUMMARY'                          => 'Resumit de vòtra comanda',
'L_EMAIL_CUST_ADDRESS'                          => 'Adreça de liurason',
'L_EMAIL_CUST_PAYMENT'                      => 'Mejan de pagament',

'L_EMAIL_SENT1'                      => 'Un corrièl de resumit de la comanda es estat mandat.',
'L_EMAIL_SENT2'                      => 'Se lo corrièl de resumit de comanda apareis pas dins vòstra lista de corrièls en espèra o qu\'aqueste corrièl es indicat coma indesirable, mercés d\'ajustar %s a vòstra lista de contactes.', // %s : adresse e-mail du gestionnaire des commandes

'L_EMAIL_ERROR1'                      => 'Una error s\'es produsida al moment de mandar vòstre corrièl de resumit.',
'L_EMAIL_ERROR2'                      => 'Una error s\'es produsida al moment de mandar la comanda per corrièl.',

'L_FILE_ORDER'                      => 'Comanda del ',

'L_FOR'                                => 'per',
'L_DEL'                                => 'Levar',

'L_MISSING_EMAIL'                      => 'L\'adreça de corrièl es pas definida.',
'L_MISSING_FIRSTNAME'                      => 'Lo prenom es pas definit.',
'L_MISSING_LASTNAME'                      => 'Lo nom d\'ostal es pas definit.',
'L_MISSING_ADDRESS'                      => 'L\'adreça es pas definida.',
'L_MISSING_ZIP'                      => 'Lo còdi postal es pas definit.',
'L_MISSING_TOWN'                      => 'La vila es pas definida.',
'L_MISSING_COUNTRY'                      => 'Lo país es pas definit.',
'L_MISSING_GIFTNAME'                      => 'Lo nom del destinatari del present es pas definit.',

#Espace public
'L_PUBLIC_ADDBASKET'                    => 'Produit ajustat al panièr',
'L_PUBLIC_BASKET'                    => 'Vòstre panièr',
'L_PUBLIC_BASKET_NIL'                    => '0.00',
'L_PUBLIC_TOTAL_BASKET'                    => 'Total',
'L_PUBLIC_NOPRODUCT'                    => 'Pas cal de produit pel moment.',
'L_PUBLIC_MANDATORY_FIELD'                    => '* = camp obligatòri',
'L_PUBLIC_FIRSTNAME'                    => 'Prenom',
'L_PUBLIC_LASTNAME'                    => 'Nom',
'L_PUBLIC_EMAIL'                    => 'Adreça de corrièl',
'L_PUBLIC_TEL'                    => 'Telefòn :',
'L_PUBLIC_ADDRESS'                    => 'Adreça postala',
'L_PUBLIC_ZIP'                    => 'Còdi postal',
'L_PUBLIC_TOWN'                    => 'Vila',
'L_PUBLIC_COUNTRY'                    => 'País',
'L_PUBLIC_GIFT'                    => 'S\'agís d\'un present ?',
'L_PUBLIC_GIFTNAME'                    => 'Mercés d\'indicar lo prenom e lo nom d\'ostal de la persona destinatària del present',
'L_PUBLIC_COMMENT'                   => 'Vòstre comentari :',
'L_PUBLIC_VALIDATE_ORDER'                   => 'Validatz la comanda',
'L_PUBLIC_ADD_BASKET'                   => 'Ajustar al panièr',
'L_PUBLIC_TAX'                   => 'T.T.C.',

#panier.js
'L_TOTAL_BASKET'                    => 'Total (fraisses de pòrt enclús)',
'L_TITRE_PANIER'                    => 'Panièr',


);
