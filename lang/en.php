<?php

$LANG = array(

'L_PAGE_TITLE'				        => 'Creation and edition of products',

'L_DEFAULT_NEW_PRODUCT_URL'			=> 'new-product',
'L_CONFIG_INFOS_NB_PRODUCTS'		=> 'Number of products :',
'L_MENU_PRODUCTS_TITLE'				=> 'List and modification of products',
'L_MENU_PRODUCTS'					=> 'List of products',
'L_MENU_CATS'					    => 'List of categories',
'L_MENU_ORDERS'					    => 'List of orders',
'L_MENU_CONFIG'					    => 'Configuration',

'L_DATE'                            => 'Date',
'L_PAIEMENT'                        => 'Payment method',
'L_MONTANT'                         => 'Amount',
'L_ACTIONS'                         => 'Actions',

'L_EMAIL'		   			        => 'Mail\'s recipient(s) *',
'L_EMAIL_CC'	   			        => 'Mail\'s recipients(s) in copy ',
'L_EMAIL_BCC'	   			        => 'Mail\'s recipient(s) in hidden copy ',
'L_EMAIL_SUBJECT'  			        => 'Subject of the mail',
'L_DEFAULT_OBJECT'			        => 'Summary of the command',
'L_DEFAULT_CONFIRMATION_OBJECT'	 	=> 'Confirmation of the command',
'L_TEMPLATE'				        => 'Template',
'L_UNKNOWN_PRODUCT'				    => 'This product doesn\'t exist or no longer exist !',
'L_PRODUCTCONTENT_INPROCESS'	    => 'This product is currently being edited',
# products.php

'L_PRODUCTS_PAGE_TITLE'				=> 'Creation and edition of products',
'L_PRODUCTS_ID'						=> 'Identifier',
'L_PRODUCTS_GROUP'					=> 'Group',
'L_PRODUCTS_TITLE'					=> 'Title',
'L_PRODUCTS_URL'					=> 'Url',
'L_PRODUCTS_ACTIVE'					=> 'Active',
'L_PRODUCTS_ORDER'					=> 'Order',
'L_PRODUCTS_MENU'					=> 'Menu',
'L_PRODUCTS_ACTION'					=> 'Action',
'L_PRODUCTS_TEMPLATE_FIELD'			=> 'Template',
'L_PRODUCTS_PAGE_HOME'				=> 'Define as welcoming product',
'L_PRODUCTS_HOME_PAGE' 			 	=> 'Welcoming<br />Product',
'L_PRODUCTS_SRC_TITLE'				=> 'Edit the source code of this product',
'L_PRODUCTS_SRC'					=> 'Edit',
'L_PRODUCTS_NEW_PAGE'				=> 'New product',
'L_PRODUCTS_UPDATE'					=> 'Modify the list of products',
'L_ERR_PRODUCT_ALREADY_EXISTS'		=> 'Product title already in use',
'L_PRODUCTS_IMAGE'          		=> 'Product image',
'L_PRODUCTS_IMAGE_CHOICE'          	=> 'Choose an image',
'L_PRODUCTS_SHORTCODE'          	=> 'shortcode to use in a static page',
'L_PRODUCTS_BASKET_BUTTON'          => 'Display the button "Add to the basket"',
'L_PRODUCTS_BASKET_NO_BUTTON'       => 'Display a message if the button "Add to the basket" is not displayed',
'L_PRODUCTS_CATEGORIES'          	=> 'Categories of this product',
'L_PRODUCTS_PRICE'           	    => 'Price tax included',
'L_PRODUCTS_WEIGHT'           	    => 'Weight (kg)',



# product.php

'L_PRODUCT_BACK_TO_PAGE'			=> 'Return to the product list',
'L_CAT_BACK_TO_PAGE'			    => 'Return to the category list',
'L_PRODUCT_UNKNOWN_PAGE'			=> 'This product doesn\'t exist or is no longer available !',
'L_PRODUCT_TITLE'					=> 'Edit the source code of this product',
'L_CAT_TITLE'					    => 'Edit the source code of this category',

'L_PRODUCT_VIEW_PAGE_ON_SITE'				=> 'View the product %s on the site',
'L_CAT_VIEW_PAGE_ON_SITE'				    => 'View the product\'s category %s on the site',

'L_PRODUCT_UPDATE'					=> 'Save this product',
'L_CAT_UPDATE'					    => 'Save this category',
'L_PRODUCT_TITLE_HTMLTAG'			=> 'Title tag contents (optional)',
'L_PRODUCT_META_DESCRIPTION'		=> '"Description" Meta tag content  for this product (optional)',
'L_CAT_META_DESCRIPTION'		    => '"Description" Meta tag content for this category (optional)',
'L_PRODUCT_META_KEYWORDS'			=> '"Keywords" Meta tag content for this product (optional)',
'L_CAT_META_KEYWORDS'			    => '"Keywords" Meta tag content for this category (optional)',

#create product/cat
'CREATE_PRODUCTS'                   => 'Creation and edition of products',
'CREATE_CATS'                       => 'Creation and edition of categories',

'L_CAT'                             => 'Category',

#commandes
'LIST_ORDERS'                       => 'List of orders',

#Related to panier
'L_PANIER_POS_BOTTOM'               => 'At the bottom of the products and categories pages',
'L_PANIER_POS_SEPARATE'             => 'On a separate page',
'L_PANIER_POS_BOTH'                 => 'At the bottom of the pages and on a separate page',

#config page
'L_CONFIG_SHOP_INFO'                     => 'Shop informations',
'L_CONFIG_SHOP_NAME'                     => 'Name of the Shop',
'L_CONFIG_SHOP_OWNER'                     => 'Last and first name of the shop owner',
'L_CONFIG_SHOP_STREET'                     => 'Address of the shop',
'L_CONFIG_SHOP_ZIP'                     => 'Post Code of the shop',
'L_CONFIG_SHOP_TOWN'                     => 'Town of the Shop',
'L_CONFIG_SHOP_CURRENCY'                     => 'Currency',
'L_CONFIG_POSITION_CURRENCY'            => 'Position of the currency',
'L_BEFORE'                              => 'Before the price',
'L_AFTER'                              => 'After the price',
'L_CONFIG_SECURITY'                 => 'Security',
'L_CONFIG_SECURITY_KEY'                 => 'Encryption key',

'L_CONFIG_DELIVERY_TITLE'                 => 'Configuration of delivery and payment methods',
'L_CONFIG_DELIVERY_SHIPPING'                 => 'Delivery using "SoColissimo Recommandé"',
'L_CONFIG_DELIVERY_CONFIG'                 => 'Configuration of "SoColissimo Recommandé"',
'L_CONFIG_DELIVERY_RECORDED'                 => 'Recorded delivery',
'L_CONFIG_DELIVERY_WEIGHT'                 => 'Weight in kg',

'L_CONFIG_PAYMENT_CHEQUE'                 => 'Payment by cheque',
'L_CONFIG_PAYMENT_CASH'                 => 'Payment in cash',
'L_CONFIG_PAYMENT_PAYPAL'                 => 'Payment by Paypal',
'L_CONFIG_CONF_PAYPAL'                 => 'Paypal configuration',
'L_CONFIG_EMAIL_PAYPAL'                 => 'Paypal email address',
'L_CONFIG_CURRENCY_PAYPAL'                 => 'Currency code',
'L_CONFIG_RETURN_URL_PAYPAL'                 => 'Return URL',
'L_CONFIG_CANCEL_URL_PAYPAL'                 => 'Cancel URL',

'L_CONFIG_EMAIL_ORDER_TITLE'                 => 'Configuration of order email',
'L_CONFIG_EMAIL_ORDER_SUBJECT_CUST'                 => 'Email subjet "Order summary" for the customer',
'L_CONFIG_EMAIL_ORDER_SUBJECT_SHOP'                 => 'Email subject (New order) for the shop owner',

'L_CONFIG_MENU_TITLE'                       => 'Configuration of menu',
'L_CONFIG_MENU_POSITION'                       => 'Position in the menu of the categories and static pages (basket)',

'L_CONFIG_PAGE'                       => 'Configuration of pages',

'L_CONFIG_BASKET_DISPLAY'              => 'Location of the basket',
'L_CONFIG_PAGE_TEMPLATE'              => 'Template for fixed pages and default templates for categories and products',

'L_CONFIG_SUBMIT'                       => 'Save',

'L_ADMIN_MODIFY'                        => 'Modify the list of',

'L_PRODUCTS'                            => 'products',
'L_CATEGORIES'                            => 'categories',
'L_NEW_PRODUCT'                            => 'New product',
'L_NEW_CATEGORY'                            => 'New category',

'L_ADMIN_ORDER_VIEW'                    => 'Display',
'L_ADMIN_ORDER_DELETE'                    => 'Delete',
'L_ADMIN_CONFIRM_DELETE'                    => 'Do you confirm the deletion of this order ?',

#plxMyShop.php
'L_PAYMENT_CHEQUE'                      => 'Cheque',
'L_PAYMENT_CASH'                      => 'Cash',
'L_PAYMENT_PAYPAL'                      => 'Paypal',

'L_EMAIL_SUBJECT'                          => 'New order for ',
'L_EMAIL_TEL'                          => 'Tel :',
'L_EMAIL_NOGIFT'                          => 'The order is not a gift',
'L_EMAIL_GIFT_FOR'                      => 'The order is a gift for',
'L_EMAIL_PRODUCTLIST'                   => 'List of products',
'L_EMAIL_TOTAL'                   => 'Total (tax included)',
'L_EMAIL_WEIGHT'                   => 'Poids',
'L_EMAIL_DELIVERY_COST'                   => 'Delivery cost',
'L_EMAIL_COMMENT'                   => 'Comment',
'L_EMAIL_CONFIRM_PAYPAL'                   => 'The order is confirmed and awaiting validation from your side at Paypal.',
'L_EMAIL_CONFIRM_CHEQUE'                   => 'The order has been validated and sent by email.',
'L_EMAIL_CONFIRM_CASH'                   => 'The order has been validated and sent by email.',

'L_EMAIL_CUST_SUBJECT'                          => 'Order summary ',
'L_EMAIL_CUST_MESSAGE1'                          => 'You just confirm an order from',
'L_EMAIL_CUST_MESSAGE2'                          => 'This order is',
'L_WAITING'                                => 'awaiting',
'L_ONGOING'                                => 'ongoing',
'L_EMAIL_CUST_MESSAGE3'                          => ' payment',
'L_EMAIL_CUST_CHEQUE'                          => 'To finalise this order please write a cheque to the order of',
'L_EMAIL_CUST_SENDCHEQUE'                          => 'Send the cheque at this address',
'L_EMAIL_CUST_CASH'                          => 'Please make your payment on delivery',
'L_EMAIL_CUST_PAYPAL'                          => 'This order will be finalised once the Paypal payment has been verified.',
'L_EMAIL_CUST_SUMMARY'                          => 'Summary of your order',
'L_EMAIL_CUST_ADDRESS'                          => 'Delivery address',
'L_EMAIL_CUST_PAYMENT'                      => 'Payment method',

'L_EMAIL_SENT1'                      => 'A summary email has just be sent to you.',
'L_EMAIL_SENT2'                      => 'If the summary email doesn\'t appear in your inbox or is flagged as Spam, please add %s to your contact list.',

'L_EMAIL_ERROR1'                      => 'An error occured during the sending of your summary email.',
'L_EMAIL_ERROR2'                      => 'An error occured during the sending of your order email.',

'L_FILE_ORDER'                      => 'Order of',

'L_FOR'                                => 'for',
'L_DEL'                                => 'Remove',

'L_MISSING_EMAIL'                      => 'The email address is not defined.',
'L_MISSING_FIRSTNAME'                      => 'The firstname is not defined.',
'L_MISSING_LASTNAME'                      => 'The last name is not defined.',
'L_MISSING_ADDRESS'                      => 'The address is not defined.',
'L_MISSING_ZIP'                      => 'The post code is not defined.',
'L_MISSING_TOWN'                      => 'The town is not defined.',
'L_MISSING_COUNTRY'                      => 'The country is not defined.',
'L_MISSING_GIFTNAME'                      => 'The name of the gift recipient is not defined.',

#Espace public
'L_PUBLIC_ADDBASKET'                    => 'Product added to the basket',
'L_PUBLIC_BASKET'                    => 'Your basket',
'L_PUBLIC_BASKET_NIL'                    => '0.00',
'L_PUBLIC_TOTAL_BASKET'                    => 'Total',
'L_PUBLIC_NOPRODUCT'                    => 'No product for the moment.',
'L_PUBLIC_MANDATORY_FIELD'                    => '* = mandatory field',
'L_PUBLIC_FIRSTNAME'                    => 'First name',
'L_PUBLIC_LASTNAME'                    => 'Last name',
'L_PUBLIC_EMAIL'                    => 'Email',
'L_PUBLIC_TEL'                    => 'Tel :',
'L_PUBLIC_ADDRESS'                    => 'Address',
'L_PUBLIC_ZIP'                    => 'Post Code',
'L_PUBLIC_TOWN'                    => 'Town',
'L_PUBLIC_COUNTRY'                    => 'Country',
'L_PUBLIC_GIFT'                    => 'Is this a gift ?',
'L_PUBLIC_GIFTNAME'                    => 'Please indicate the first and last name of the recipient of the gift',
'L_PUBLIC_COMMENT'                   => 'Your comment :',
'L_PUBLIC_VALIDATE_ORDER'                   => 'Confirm the order',
'L_PUBLIC_ADD_BASKET'                   => 'Add to the basket',
'L_PUBLIC_TAX'                   => 'Tax included',

#panier.js
'L_TOTAL_BASKET'                    => 'Total (delivery cost included)',
'L_TITRE_PANIER'                    => 'Basket',


);
